package com.company;

/**
 * Created by Marcin on 2017-10-06.
 */
public class Season extends Episode {
    private String seasonName;

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public String getNumberOfSeason() {
        return numberOfSeason;
    }

    public void setNumberOfSeason(String numberOfSeason) {
        this.numberOfSeason = numberOfSeason;
    }

    private String numberOfSeason;

}
