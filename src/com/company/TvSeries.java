package com.company;

import java.util.Date;
import java.util.List;

/**
 * Created by Marcin on 2017-10-06.
 */
public class TvSeries {
    private String seriesName;
    private List listOfSeasons;
    private Date dateOfRelease;

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public List getListOfSeasons() {
        return listOfSeasons;
    }

    public void setListOfSeasons(List listOfSeasons) {
        this.listOfSeasons = listOfSeasons;
    }

    public Date getDateOfRelease() {
        return dateOfRelease;
    }

    public void setDateOfRelease(Date dateOfRelease) {
        this.dateOfRelease = dateOfRelease;
    }
}
