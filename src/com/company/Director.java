package com.company;

import java.util.Date;

/**
 * Created by Marcin on 2017-10-06.
 */
public class Director {
    private String directorFirstName;
    private String directorLastName;
    private Date   directorBirthday;
    private String directorBiography;

    public String getDirectorFirstName() {
        return directorFirstName;
    }

    public void setDirectorFirstName(String directorFirstName) {
        this.directorFirstName = directorFirstName;
    }

    public String getDirectorLastName() {
        return directorLastName;
    }

    public void setDirectorLastName(String directorLastName) {
        this.directorLastName = directorLastName;
    }

    public Date getDirectorBirthday() {
        return directorBirthday;
    }

    public void setDirectorBirthday(Date directorBirthday) {
        this.directorBirthday = directorBirthday;
    }

    public String getDirectorBiography() {
        return directorBiography;
    }

    public void setDirectorBiography(String directorBiography) {
        this.directorBiography = directorBiography;
    }
}
